/*
   Copyright 2012 Verilab, Inc.
   All Rights Reserved Worldwide
*/

// RTL implementation of a parameterized width sync counter with (in order of
// precedence) async reset, sync clear, sync load, sync enable.

module counter #(WIDTH=32) (
  input  clk,
  input  rst_n,
  input  clr,
  input  ld,
  input  ena,
  input  [WIDTH-1:0] d_in,
  output reg [WIDTH-1:0] d_out
);

  always_ff @(posedge clk or negedge rst_n) begin
    if (rst_n == 1'b0) begin
      d_out <= '0;
    end
    else if (clr == 1'b1) begin
      d_out <= '0;
    end
    else if (ld == 1'b1) begin
      d_out <= d_in;
    end
    else if (ena == 1'b1) begin
      d_out <= d_out + 1;
    end
  end

endmodule
