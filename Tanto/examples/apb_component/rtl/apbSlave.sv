/*
   Copyright 2012 Verilab, Inc.
   All Rights Reserved Worldwide
*/

module apbSlave #(ADDR_WIDTH=16, DATA_WIDTH=32) (

  // The "bus" side.

  // Pick port list A or B. The choice has implications elsewhere. If using an
  // interface (A), then individual apb signals must be referenced
  // hierarchically.

  // Here's port list A, uncomment to use:
  //apb_if #(.ADDR_WIDTH(ADDR_WIDTH), .DATA_WIDTH(DATA_WIDTH)) apb,

  // Here's port list B, uncomment to use:
  input  pclk,
  input  [ADDR_WIDTH-1:0] paddr,
  input  pwrite,
  input  psel,
  input  penable,
  input  [DATA_WIDTH-1:0] pwdata,
  output logic [DATA_WIDTH-1:0] prdata,
  output logic pready,
  output logic pslverr,

  // The "interior" side.
  output logic [ADDR_WIDTH-1:0] address,
  output logic writeCmd,
  output logic readCmd,
  output logic [DATA_WIDTH-1:0] writeData,
  input  [DATA_WIDTH-1:0] readData,
  input  ack,
  input  error
);

  // The structure of this logic is entirely combinational. The apb protocol is
  // designed with generous timing margins so as to not require pipeline logic
  // in slaves.

  always_comb begin
    // Interior side.
    address = {ADDR_WIDTH{psel}} & paddr;
    writeCmd = psel && penable && pwrite;
    readCmd = psel && penable && !pwrite;
    writeData = {DATA_WIDTH{psel}} & pwdata;

    // Bus side.
    prdata = readCmd ? readData : 'h0;
    pready = psel && penable && ack;
    pslverr = psel && penable && error;
  end

endmodule
