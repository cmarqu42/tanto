class apb_env extends uvm_env;
   apb_agent         m_apb_agent;
   vlb_sys_demo_map1 regmodel;
   reg2apb_adapter   m_stim_adapter;
   uvm_reg_predictor#(apb_rw) m_predictor;
   
   `uvm_component_utils(apb_env)

       
   function new(string name = "apb_env", uvm_component parent);
      super.new(name, parent);
   endfunction: new
   
   virtual function void build_phase(uvm_phase phase);
      super.build();
      m_apb_agent  = apb_agent::type_id::create("m_apb_agent", this);
      if(regmodel == null) begin
	 regmodel = vlb_sys_demo_map1::type_id::create("regmodel",, get_full_name());
	 regmodel.build();
	 regmodel.lock_model();	 
      end
      m_predictor = uvm_reg_predictor#(apb_rw)::type_id::create("m_predictor", this);      
   endfunction: build_phase

   virtual function void connect_phase(uvm_phase phase);
      if(regmodel.get_parent() ==null) begin
      	 m_stim_adapter = reg2apb_adapter::type_id::create("stim_adapter",, get_full_name());
      	 regmodel.default_map.set_sequencer(m_apb_agent.sqr, m_stim_adapter);
	 regmodel.default_map.set_auto_predict(1);
      end
      `uvm_info(get_full_name(), "Connect phase complete", UVM_LOW)
   endfunction: connect_phase

endclass: apb_env
