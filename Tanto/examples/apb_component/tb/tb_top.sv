/*
   Copyright 2012 Verilab, Inc.
   All Rights Reserved Worldwide
*/

`include "apb.sv"
`timescale 1ns/1ps
module tb_top;
   
   import uvm_pkg::*;
   import apb_pkg::*;
   import apb_test_pkg::*;
   
   logic pclk;
   logic reset_n;
   
   apb_if apbif(.pclk(pclk));
   
   apb_demo_top #(.ADDR_WIDTH(14)) dut (
      .rst_n(reset_n),
      .pclk(apbif.pclk),
      .paddr(apbif.paddr[13:0]),
      .pwrite(apbif.pwrite),
      .psel(apbif.psel),
      .penable(apbif.penable),
      .pwdata(apbif.pwdata),
      .prdata(apbif.prdata),
      .pready(apbif.pready),
      .pslverr(apbif.pslverr)
   );


   // Reset
   initial begin
      reset_n <= 0;
      repeat (10)@(posedge pclk);
      reset_n <= 1;
   end
   // Clock
   initial begin
      pclk = 0;
      forever pclk = #0.5 ~pclk;
   end

   initial begin
      $dumpfile("apb_demo.vcd");
      $dumpvars(0, tb_top);
      
   end
   
   // Virtual Interface
   initial begin
      uvm_config_db#(apb_vif)::set(null, "*.m_apb_agent*", "vif", apbif);
      run_test();
      
   end

endmodule: tb_top

