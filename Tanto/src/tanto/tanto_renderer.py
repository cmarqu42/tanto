'''
Created on Jun 28, 2012

Copyright 2012 Verilab, Inc.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
   
@author: gmcgregor
'''

from logging import info, debug, warning, critical
from mako.template import Template
import os.path
import os
  
class TantoRenderer(object):
    def __init__(self, parent):
        self.parent = parent
    
    def _setup_output(self, template):
        try:
            self.output_template_directory = self.parent.config.get(template, 'output_directory')
        except:
            self.output_template_directory = ''
            
        self.output_path = os.path.join( self.parent.config.get('tanto', 'output_directory'), self.output_template_directory)

        try:
            self.basename = self.parent.config.get('tanto', 'basename')
        except:
            critical("Basename not defined for output files, please define this in a configuration file.")
                    
        try:
            self.filename_suffix = self.parent.config.get(template, 'filename_suffix')
        except:
            self.filename_suffix = ''
            
        try:
            self.extension = self.parent.config.get(template, 'extension')
        except:
            self.extension = 'out'
            
        self.output_filename =  self.output_path + '/%s%s.%s' % (self.basename, self.filename_suffix, self.extension)
        info('Rendering %s to %s' % (template, self.output_filename)) 
        if not os.path.exists(self.output_path):
            debug("Making output directory: %s" % (self.output_path))
            os.makedirs(self.output_path)     
        
        
    def render(self, template):
        template_name = '%s/%s.template' % (template, template)
        debug('Template %s' % (template_name))
        mako_renderer = self.parent.template_lookup.get_template(template_name)
        self._setup_output(template)
                            
        f=open(self.output_filename, 'w')
        
        f.write(mako_renderer.render(self.parent.data, self.parent.config).encode("UTF-8"))
        f.close()
               
              
            