'''
Created on Jun 28, 2012

Copyright 2012 Verilab, Inc.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
   
@author: gmcgregor
'''

from tanto.tanto import TantoRenderer

from logging import info, debug, warning
from openpyxl import Workbook
from openpyxl.cell import get_column_letter


class ExcelRenderer(TantoRenderer):
    
    def render(self, template):
        self._setup_output(template)
    
        wb = Workbook()
        ws = wb.worksheets[0]
        reg_model = self.parent.data
        
        for maps in reg_model['memoryMaps']:
            for map in maps['memoryMap']:
                
                ws.title = map.name
                row_index = 1
                for address_block in map['addressBlock']:
                    ws.cell('%s%s' % ('A', row_index)).value = address_block.name  
                    ws.cell('%s%s' % ('D', row_index)).value = address_block.baseAddress  
                                      
                    for register in address_block['register']:
                        ws.cell('%s%s' % ('B', row_index)).value = register.name
                        ws.cell('%s%s' % ('E', row_index)).value = register.addressOffset
                        
                        for field in register['field']:
                            ws.cell('%s%s' % ('C', row_index)).value = field.name
                            ws.cell('%s%s' % ('F', row_index)).value = field.bitOffset
                            row_index += 1   
                                                 
                        row_index += 1
                    row_index += 1
                        
        wb.save(filename = self.output_filename)  
