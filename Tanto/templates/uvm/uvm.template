<%page args="reg_model, config"/>
<% # Load some template variables from the [uvm] section in the appropriate config files
class_prefix = config.get('uvm', 'class_prefix')
insert_comments = eval(config.get('uvm', 'insert_comments'))
coverage = eval(config.get('uvm', 'coverage'))
%>

<%!
from logging import warning, debug, info
# This is a top level block of code - I'm adding several helper functions that are callable from anywhere within this template/Context

# TODO needs all access types encoded here (see uvm/distrib/src/reg/uvm_reg_field.svh line 189- for details
access_types = {'read-write': 'RW', 'write-only': 'WO', 'writeOnce': 'WO1', 'read-writeOnce':'W1', 'read-only': 'RO'}

def hex_to_sv(value, size='32', offset='0'):
  '''Convert an 0xdeadbeef format hex value to an SV size'hdeadbeef'''
  # TODO Also should be moved out into a reusable library (?)
  value = (eval(value)>>eval(offset)) & (1<<eval(size))-1
  return size + "'h%x" % (value)
  
def remove_reserved(element, subfield):
  '''Strip reserved fields'''
  # TODO - should really parse the vendorExtension is_rsv I think
  items = element[subfield]
  return [x for x in items if x.name != 'reserved']
  
def get_access_type(register, field=None):  
  '''Convert longform text access type to shortform (e.g., read-write to RW)'''
  reg_access = None
  field_access = None

  try:
    reg_access = register.access
    reg_access = access_types[reg_access]
    reg_name = register.name
    field_access = field.access
    field_access = access_types[field_access]
    field_name = field.name
  except:
    pass 
    
  if field_access:
    return field_access
  if reg_access:
    return reg_access
  if field:
    info("UNDEFINED REGISTER ACCESS TYPE for %s_%s" % (register.name, field.name))
  else:
    info("UNDEFINED REGISTER ACCESS TYPE for %s" % (register.name))
  return 'RW'

def  is_field_writeable(register, field):
    if get_access_type(register, field).find('W') != -1:
        return True
    else:
        return False

def is_field_readable(register, field):
    if get_access_type(register, field).find('R') != -1:
        return True
    else:
        return False
      
    
def is_field_in_range(field, byte_boundary):

  in_range = True
  offset = eval(field.bitOffset)
  width = eval(field.bitWidth)
  upper_bound = offset+width-1
  
  if upper_bound < (byte_boundary*8): # upper end of field is below byte boundary of concern
    in_range = False
      
  if offset >= ((byte_boundary+1)*8):  # lower end of field is above byte boundary of concern
    in_range = False

  return in_range  # must be some overlap
   
def is_field_unique_in_block(address_block, test_register, test_field):

  test_name = test_field.name
  for register in address_block['register']:
    if register.name == test_name:
      if len(address_block['register']) == 1 and len(register['field']) == 1:
        return True # special case for a single register with a single field with the same name
      else:
        return False
    if register.name == test_register.name:
      continue
    for field in remove_reserved(register, 'field'):
      if test_name == field.name:
        return False
    
  return True
   
def is_field_individually_accessible(register, test_field):
       
  lower_byte_boundary = eval(test_field.bitOffset) / 8
  in_play_fields = [field for field in remove_reserved(register, 'field') if is_field_in_range(field, lower_byte_boundary)]
  
  if len(in_play_fields) == 1:
    return '1'
  else:
    return '0'
  
def get_reset_value(register):
  try:
    reset = register['reset'][0].value
  except:
    reset = '0x0'
  return reset
  
def has_no_fields(register):
    if len(remove_reserved(register, 'field')):
        return False
    return True

def description_comment(element, active):
    if not active:
        return ''
    try:
        return "// %s" % element.description.replace('\n','')
    except:
        return ''

def get_bit_width(field):
    offset = eval(field.bitOffset)
    width = eval(field.bitWidth)
    if width == 1:
        return "[%d]" % offset
    else:
        return "[%d:%d]" % (offset+width-1, offset)
        
def create_bit_coverage(field, bit, val, rnw):
    size = eval(field.bitWidth)
    bit = bit - eval(field.bitOffset)
    bit_string = ('?'* (size-bit-1)) + val + ('?' * (bit))
    return '{%d\'b%s%s}' % (size+1, bit_string, rnw == 'read' and '1' or '0')                
              
%>

//   _   ___     ____  __   _                       _       _       
//  | | | \ \   / /  \/  | | |_ ___ _ __ ___  _ __ | | __ _| |_ ___ 
//  | | | |\ \ / /| |\/| | | __/ _ \ '_ ` _ \| '_ \| |/ _` | __/ _ \.
//  | |_| | \ V / | |  | | | ||  __/ | | | | | |_) | | (_| | ||  __/
//   \___/   \_/  |_|  |_|  \__\___|_| |_| |_| .__/|_|\__,_|\__\___|
//                                           |_|                    
// Generated by Tanto
// 2012 Verilab, Inc.
//
% for memory_maps in reg_model['memoryMaps']:
%   for memory_map in memory_maps['memoryMap']:
 
`ifndef ${class_prefix.upper()}${memory_map.name.upper()}
`define ${class_prefix.upper()}${memory_map.name.upper()}

import uvm_pkg::*;

%     for address_block in memory_map['addressBlock']:
%       for register in address_block['register']:
 ${description_comment(register, insert_comments)}
class ${class_prefix}reg_${memory_map.name}_${address_block.name}_${register.name} extends uvm_reg;
%         for field in remove_reserved(register, 'field'):
  ${get_access_type(register, field)=='RW' and 'rand' or ''} uvm_reg_field ${field.name}; ${description_comment(field, insert_comments)}
%         endfor # field
%         if has_no_fields(register):
  ${get_access_type(register)=='RW' and 'rand' or ''} uvm_reg_field ${register.name};
%         endif

% if coverage:
  local uvm_reg_data_t m_current;
  local uvm_reg_data_t m_data;
  local uvm_reg_data_t m_be;
  local bit m_is_read;
  
  covergroup cg_bits();
    option.per_instance = 1;
%         for field in remove_reserved(register, 'field'):
    ${field.name}: coverpoint {m_current${get_bit_width(field)}} iff (!m_is_read && m_be) {
%           for bit in xrange(eval(field.bitOffset), eval(field.bitOffset) + eval(field.bitWidth) ):
% if is_field_readable(register, field):
        wildcard bins bit_${bit-eval(field.bitOffset)}_wr_as_0 = ${create_bit_coverage(field, bit, '0', 'write')};
        wildcard bins bit_${bit-eval(field.bitOffset)}_wr_as_1 = ${create_bit_coverage(field, bit, '1', 'write')};
% else:
        wildcard bins bit_${bit-eval(field.bitOffset)}_wr = ${create_bit_coverage(field, bit, '?', 'write')};
% endif
% if is_field_writeable(register, field):
        wildcard bins bit_${bit-eval(field.bitOffset)}_rd_as_0 = ${create_bit_coverage(field, bit, '0', 'read')};
        wildcard bins bit_${bit-eval(field.bitOffset)}_rd_as_1 = ${create_bit_coverage(field, bit, '1', 'read')};
% else:
        wildcard bins bit_${bit-eval(field.bitOffset)}_rd = ${create_bit_coverage(field, bit, '?', 'read')};
% endif # is_field_writeable
%           endfor # bit
      option.weight = ${eval(field.bitWidth) * ( (is_field_writeable(register, field) and 2 or 1) +  (is_field_readable(register, field) and 2 or 1) )};
    }

%         endfor # field
  endgroup
% endif # coverage

  function new(string name = "${memory_map.name}_${address_block.name}_${register.name}");
    super.new(name, 32, ${coverage and 'UVM_CVR_REG_BITS' or 'UVM_NO_COVERAGE'});
% if coverage:
  if (has_coverage(UVM_CVR_REG_BITS))
    cg_bits = new();
% endif # coverage
  endfunction: new
  
  virtual function void build();
%         for field in remove_reserved(register, 'field'):
      this.${field.name} = uvm_reg_field::type_id::create("${field.name}");
      this.${field.name}.configure(this, ${field.bitWidth}, ${field.bitOffset}, "${get_access_type(register, field)}", 0, ${hex_to_sv(get_reset_value(register), field.bitWidth, field.bitOffset)}, 1, 0, ${is_field_individually_accessible(register, field)});
%         endfor # field
%         if has_no_fields(register):
      this.${register.name} = uvm_reg_field::type_id::create("${register.name}");
      this.${register.name}.configure(this, ${register.size}, 0, "${get_access_type(register)}", 0, ${hex_to_sv(get_reset_value(register), register.size)}, 1, 0, 1);
%         endif
   endfunction: build
   
  `uvm_object_utils(${class_prefix}reg_${memory_map.name}_${address_block.name}_${register.name})

% if coverage:
  virtual function void sample(uvm_reg_data_t data,
                               uvm_reg_data_t byte_en,
                               bit            is_read,
                               uvm_reg_map    map);
     if (get_coverage(UVM_CVR_REG_BITS)) begin
        m_current = get();
        m_data    = data;
        m_be      = byte_en;
        m_is_read = is_read;
        cg_bits.sample();
     end
  endfunction
% endif # coverage
endclass : ${class_prefix}reg_${memory_map.name}_${address_block.name}_${register.name}


%       endfor # register

class ${class_prefix}block_${memory_map.name}_${address_block.name} extends uvm_reg_block;
%       for register in address_block['register']:
  rand ${class_prefix}reg_${memory_map.name}_${address_block.name}_${register.name} ${register.name};
%       endfor # register
% if coverage:
  local uvm_reg_data_t m_offset;
% endif # coverage
%       for register in address_block['register']:
%         for field in remove_reserved(register, 'field'):
  ${get_access_type(register, field)=='RW' and 'rand' or ''} uvm_reg_field ${register.name}_${field.name};
% if is_field_unique_in_block(address_block, register, field):
  ${get_access_type(register, field)=='RW' and 'rand' or ''} uvm_reg_field ${field.name};
% endif
%         endfor # field
%         if has_no_fields(register):
  ${get_access_type(register)=='RW' and 'rand' or ''} uvm_reg_field ${register.name}_${register.name};
%         endif
%       endfor # register

% if coverage:
  covergroup cg_addr();
    option.per_instance = 1;
%   for register in address_block['register']:
  ${register.name} : coverpoint m_offset {
    bins accessed  = { `UVM_REG_ADDR_WIDTH'h${register.addressOffset}};
    option.weight = 1;
  }
  
%   endfor # register  
  
  endgroup
  
% endif # coverage

  function new(string name = "${memory_map.name}_${address_block.name}");
    super.new(name, build_coverage(${coverage and 'UVM_CVR_ADDR_MAP' or 'UVM_NO_COVERAGE'}));
% if coverage:
    cg_addr = new();
% endif #coverage
  endfunction: new

   virtual function void build();
      this.default_map = create_map("", 0, 4, UVM_LITTLE_ENDIAN);
%       for register in address_block['register']:      
      this.${register.name} = ${class_prefix}reg_${memory_map.name}_${address_block.name}_${register.name}::type_id::create("${register.name}");
      this.${register.name}.build();
      this.${register.name}.configure(this, null, "");
      this.default_map.add_reg(this.${register.name}, `UVM_REG_ADDR_WIDTH'h${register.addressOffset}, "RW", 0);
%         for field in remove_reserved(register, 'field'):
      this.${register.name}_${field.name} = this.${register.name}.${field.name};
%         if is_field_unique_in_block(address_block, register, field):
      this.${field.name} = this.${register.name}.${field.name};
%         endif
%         endfor # field
%         if has_no_fields(register):
      this.${register.name}_${register.name} = this.${register.name}.${register.name};
%         endif
%       endfor # register
      this.lock_model();
   endfunction : build
  `uvm_object_utils(${class_prefix}block_${memory_map.name}_${address_block.name})

% if coverage:
    function void sample(uvm_reg_addr_t offset,
                         bit            is_read,
                         uvm_reg_map    map);
      if (get_coverage(UVM_CVR_ADDR_MAP)) begin
        m_offset = offset;
        cg_addr.sample();
      end
    endfunction
% endif #coverage

endclass : ${class_prefix}block_${memory_map.name}_${address_block.name}

%     endfor # address_block
class ${class_prefix}sys_${memory_map.name} extends uvm_reg_block;
    
%     for address_block in memory_map['addressBlock']:
   rand ${class_prefix}block_${memory_map.name}_${address_block.name} ${address_block.name};
%     endfor # address_block

  function new(string name = "${memory_map.name}");
    super.new(name);
  endfunction: new

  function void build();
      this.default_map = create_map("", 0, 4, UVM_LITTLE_ENDIAN);
%     for address_block in memory_map['addressBlock']:
      this.${address_block.name} = ${class_prefix}block_${memory_map.name}_${address_block.name}::type_id::create("${address_block.name}");
      this.${address_block.name}.configure(this, "");
      this.${address_block.name}.build();
      this.default_map.add_submap(this.${address_block.name}.default_map, `UVM_REG_ADDR_WIDTH'h${address_block.baseAddress});
%     endfor # address_block
    this.lock_model();
  endfunction : build

  `uvm_object_utils(${class_prefix}sys_${memory_map.name})
endclass : ${class_prefix}sys_${memory_map.name}
 
%   endfor  # memory_map
% endfor  # memory_maps

`endif // ${class_prefix.upper()}${memory_map.name.upper()}







